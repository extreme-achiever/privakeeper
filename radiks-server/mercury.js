/* eslint-disable */
const express = require('express');
const cookieParser = require('cookie-parser')();
const cors = require('cors')({
  "origin": "*",
  "methods": "GET,HEAD,PUT,PATCH,POST,DELETE",
  "preflightContinue": false,
  "optionsSuccessStatus": 204
});

const path = require('path');
const { setup } = require('radiks-server');

const appConfig = []; /* should be { path, handler(req, res) }*/

const addHandler = (path, handler) => {
    appConfig.push({
        path,
        handler
    });
};

const initHttpClient = () => {
    const app = express();

    app.use(cors);
    app.use(cookieParser);

    // register
    appConfig.map(config => app.post(config.path, config.handler));

    // add radiks
    setup().then(RadiksController => {
      app.use('/radiks', RadiksController);
    });

    app.use('/worker', express.static(path.join(__dirname, 'worker')))


    return app;
};

const port = process.env.PORT || 8000;
const server = initHttpClient();
server.listen(port, function() {
    console.log('server listening on port ' + port)
})
