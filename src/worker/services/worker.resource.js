const workerResource = {}

export const workerResourceService = {
  get (resourceName, defaultValue) {
    if (workerResource[resourceName] === undefined) {
      workerResource[resourceName] = defaultValue
    }

    return workerResource[resourceName]
  },
  set (resourceName, value) {
    workerResource[resourceName] = value
  }
}
