import { batchUpload } from '../../services/utils'

// eslint-disable-next-line camelcase
const done_handleFileBuffer = (worker, fileInfo, { fileInstance }) => {
  console.debug('done_handleFileBuffer', 'triggered')
  // when done handle buffer, save to radiks and upload to Gaia
  fileInstance.createFile(fileInfo).then((fileInfoSaved) => {
    console.log('created file info')
    console.log(fileInfoSaved)
    worker.postMessage({ taskName: 'getLatestBufferBatch', data: { fileInfo: fileInfoSaved } })
  })
}

// eslint-disable-next-line camelcase
const done_getLatestBufferBatch = (worker, { fileId, buffer, id, fragments }, { fileInstance }) => {
  console.debug('done_getLatestBufferBatch', 'triggered')

  console.log('fileId', fileId)

  const { fileHandlerMapper } = fileInstance.$data

  const currentFileHandler = fileHandlerMapper[fileId]

  // eslint-disable-next-line no-unused-vars
  const { handleAbort, error, progressHandler, removeFileLocal, fileRecord } = currentFileHandler

  handleAbort(() => new Promise(resolve => {
    console.log('abort file upload')
    console.log(fileId, id, fragments)
    worker.postMessage({ taskName: 'abortUpload', data: { fileId } })
    fileInstance.setFileToProgressBar({ fileId, status: 'aborting', fileName: fileRecord.fileName })
    return fileInstance.deleteFileRecord({ fileId, id, fragments }).then(() => {
      removeFileLocal(id)
      fileInstance.removeProgress({ fileId })
    }).finally(resolve)
  }))

  batchUpload(fileId, buffer, progressHandler(fragments)).then(() => {
    worker.postMessage({ taskName: 'getLatestBufferBatch', data: { fileInfo: { fileId, id, fragments } } })
  }).catch((err) => {
    error(err)
  })
}

// eslint-disable-next-line camelcase
const done_Buffer = async (worker, { fileId, bufferStatus, id, fileInfo }, { fileInstance }) => {
  console.debug('buffer is empty', 'triggered')
  console.debug('buffer map status', bufferStatus.length)

  const updateFileStatus = fileInstance.updateFileStatus

  if (bufferStatus.indexOf(fileId) === -1) {
    const { fileHandlerMapper } = fileInstance.$data

    const currentFileHandler = fileHandlerMapper[fileId]

    // eslint-disable-next-line no-unused-vars
    const { load } = currentFileHandler
    updateFileStatus({ id, status: 'done' }).then(() => {
      fileInstance.removeProgress({ fileId })
      load(fileInfo)
    })
  }

  // if (bufferStatus.length === 0) fileInstance.fetchFiles()
}

export default {
  done_handleFileBuffer,
  done_getLatestBufferBatch,
  done_Buffer
}
