import _ from 'lodash'
import { workerResourceService } from './worker.resource'
// eslint-disable-next-line no-unused-vars
// import { encryptECIES, decryptECIES } from 'blockstack/src/encryption/ec'
// const WORKER_OVERHEAD_LIMIT = 0.05e+7 // 500Kb

const handleFileBuffer = (worker, { fileInfo, fileBuffer }, { MAX_FILE_SIZE, MAX_CONCURRENT_LIMIT }) => {
  // console.log(fileBuffer)
  const buffer = Buffer.from(fileBuffer) // convert to buffer

  const chunksBuffer = _.chunk(buffer, MAX_FILE_SIZE)
    .map((chunkedBuffer, index) => ({ order: index, chunkedBuffer: Buffer.from(chunkedBuffer).buffer })) // chunk buffer

  const bufferMap = workerResourceService.get('bufferMap', {})

  bufferMap[fileInfo.fileId] = _.chunk(chunksBuffer, MAX_CONCURRENT_LIMIT).reverse()

  console.log('buffer map status: ', Object.keys(bufferMap).length)

  worker.postMessage({
    taskName: `done_handleFileBuffer`, data: { ...fileInfo, fragments: chunksBuffer.length }
  })
}

const getLatestBufferBatch = (worker, { fileInfo }) => {
  const bufferMap = workerResourceService.get('bufferMap')

  const { fileId, id, fragments } = fileInfo

  console.log(fileId)
  if (bufferMap[fileId].length > 0) {
    const buffer = bufferMap[fileId].pop()

    const bufferData = buffer.map(elm => elm.chunkedBuffer)

    console.debug('before sent buffer to main thread', bufferData.map(buffer => buffer.byteLength))

    // this should be mini batches handling
    worker.postMessage({
      taskName: `done_getLatestBufferBatch`,
      data: {
        fileId,
        id,
        fragments,
        buffer
      }
    }, [...bufferData])

    console.debug('after sent buffer to main thread', bufferData.map(buffer => buffer.byteLength))
  } else {
    delete bufferMap[fileId]

    const bufferStatus = Object.keys(bufferMap)

    console.debug('done fetch buffer in worker thread. BufferMap', bufferStatus)

    worker.postMessage({
      taskName: `done_Buffer`,
      data: {
        fileId,
        id,
        fileInfo,
        bufferStatus
      }
    })
  }
}

export const abortUpload = (worker, { fileId }) => {
  const bufferMap = workerResourceService.get('bufferMap')
  delete bufferMap[fileId]
}

// https://stackoverflow.com/a/39257212/6182992
export const storeBufferBatch = (worker, { fileInfo, bufferInfo }) => {
  const bufferDownloadBatch = workerResourceService.get('bufferDownloadBatch', {})

  const { fileId } = fileInfo

  const { bufferBatch, order } = bufferInfo

  if (bufferDownloadBatch[fileId] === undefined) bufferDownloadBatch[fileId] = {}

  bufferDownloadBatch[fileId][order] = bufferBatch
}

export default {
  handleFileBuffer,
  getLatestBufferBatch
}
