// Worker.js
import UploadWorkerService from '../services/upload.workerside'

self.addEventListener('message', (event) => {
  // very large data here
  // eslint-disable-next-line no-unused-vars
  const { data, taskName, options } = event.data

  // eslint-disable-next-line import/namespace
  if (taskName in UploadWorkerService) UploadWorkerService[taskName](self, data, options)
})
