import { RandomColor } from '../services/utils'
import jquery from 'jquery'
import 'jquery.scrollto'

export default async ({ Vue }) => {
  Vue.prototype.$RandomColor = RandomColor
  Vue.prototype.$ = jquery
}
