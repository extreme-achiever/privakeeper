// import something here
import VueClipboard from 'vue-clipboard2'

// "async" is optional
export default async ({ Vue }) => {
  VueClipboard.config.autoSetContainer = true // add this line
  Vue.use(VueClipboard)
}
