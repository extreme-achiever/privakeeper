// import something here
import * as radiks from '../services/radiks'
// "async" is optional
export default async ({ Vue }) => {
  Vue.prototype.$user = radiks.AppUser
  Vue.prototype.$userSession = radiks.AppUserSession
  Vue.prototype.$getConfig = radiks.getConfig
}
