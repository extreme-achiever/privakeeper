import { generateSecret, generateToken, authenticator } from '../services/otp'

export default async ({ Vue }) => {
  Vue.prototype.$generateToken = generateToken
  Vue.prototype.$generateSecret = generateSecret
  Vue.prototype.$authenticator = authenticator
}
