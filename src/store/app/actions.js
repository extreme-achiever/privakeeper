import { AppUserSession, AppUser } from '../../services/radiks'
import JSSoup from 'jssoup'
import { TwoFactorSeed, PasswordSeed, Note, FileRecord } from '../../services/models'
import { authenticator } from '../../services/otp'
import { deleteChunkedBuffer } from '../../services/utils'

export const startTimer = (context) => {
  const timer = setInterval(() => {
    context.commit('setRemainingOTPTime', authenticator.timeRemaining())
  }, 1000)

  return () => clearInterval(timer)
}
export const handleSignin = async (context, callback) => {
  context.commit('setLoading', true)

  try {
    if (AppUserSession.isSignInPending()) {
      await AppUserSession.handlePendingSignIn()
      await AppUser.createWithCurrentUser()
      await context.dispatch('initializeData')
      console.log('user is logged in')
      if (typeof callback === 'function') callback()
    }
  } catch (e) {
    console.log(e)
  }

  context.commit('setLoading', false)
}

export const signOut = (context) => {
  console.log('signout')

  AppUserSession.signUserOut()

  window.location.reload()
}

export const initializeData = async (context) => {
  context.commit('setLoading', true)

  try {
    await (new Promise(resolve => {
      setTimeout(async () => {
        context.commit('setLoading', true)
        // this to fetch demo data

        if (AppUserSession.isUserSignedIn()) {
          // fetch user information
          const userData = AppUserSession.loadUserData()
          await AppUser.createWithCurrentUser()
          context.commit('setUser', userData)

          // reset data
          context.commit('resetProgress')
          // fetch user data
          context.dispatch('fetchTwoFactorSeed')
          context.dispatch('fetchPasswordSeed')
          context.dispatch('fetchNotes')
          context.dispatch('fetchFiles')
        } else {
          context.commit('clearState')
        }

        resolve()
      }, 1000)
    }))
  } catch (er) {
    console.log(er)
  }

  context.commit('setLoading', false)
}

export const handleAppReady = async (context) => {
  await context.dispatch('initializeData')
  await context.dispatch('handleSignin')
}

// two factor seed
export const fetchTwoFactorSeed = (context) => {
  context.commit('setLoading', true)

  return new Promise(resolve => {
    return TwoFactorSeed.fetchOwnList().then(results => {
      const twoFactorSeedData = results.map(elm => parseData(elm))

      context.commit('setTwoFactorSeed', twoFactorSeedData)
      return resolve(twoFactorSeedData)
    }).finally(() => {
      context.commit('setLoading', false)
    })
  })
}

export const createTwoFactorSeed = (context, data) => {
  context.commit('setLoading', true)

  return new Promise(resolve => {
    const twoFactorSeedData = new TwoFactorSeed({ data: JSON.stringify(data) })

    return twoFactorSeedData.save().then(resolve).finally(() => {
      context.dispatch('fetchTwoFactorSeed')
    })
  })
}

export const deleteTwoFactorSeed = (context, twoFactorSeedId) => {
  context.commit('setLoading', true)

  return new Promise(resolve => {
    return TwoFactorSeed.findById(twoFactorSeedId).then(twoFactorSeedData => twoFactorSeedData.destroy()).then(resolve).finally(() => {
      context.dispatch('fetchTwoFactorSeed')
    })
  })
}

export const updateTwoFactorSeed = (context, { id, data }) => {
  context.commit('setLoading', true)

  return new Promise(resolve => {
    return TwoFactorSeed.findById(id).then(twoFactorSeed => {
      twoFactorSeed.update({ data: JSON.stringify(data) })
      return twoFactorSeed.save()
    }).then(resolve).finally(() => {
      context.dispatch('fetchTwoFactorSeed')
    })
  })
}

// password seed
export const fetchPasswordSeed = (context) => {
  context.commit('setLoading', true)

  return new Promise(resolve => {
    return PasswordSeed.fetchOwnList().then(results => {
      const passwordSeedData = results.map(elm => parseData(elm))

      context.commit('setPasswordSeed', passwordSeedData)
      return resolve(passwordSeedData)
    }).finally(() => {
      context.commit('setLoading', false)
    })
  })
}

export const createPasswordSeed = (context, data) => {
  context.commit('setLoading', true)

  return new Promise(resolve => {
    const passwordSeedData = new PasswordSeed({ data: JSON.stringify(data) })

    return passwordSeedData.save().then(resolve).finally(() => {
      context.dispatch('fetchPasswordSeed')
    })
  })
}

export const deletePasswordSeed = (context, passwordSeedId) => {
  context.commit('setLoading', true)

  return new Promise(resolve => {
    return PasswordSeed.findById(passwordSeedId).then(passwordSeed => passwordSeed.destroy()).then(resolve).finally(() => {
      context.dispatch('fetchPasswordSeed')
    })
  })
}

export const updatePasswordSeed = (context, { id, data }) => {
  context.commit('setLoading', true)

  return new Promise(resolve => {
    return PasswordSeed.findById(id).then(passwordSeed => {
      passwordSeed.update({ data: JSON.stringify(data) })
      return passwordSeed.save()
    }).then(resolve).finally(() => {
      context.dispatch('fetchPasswordSeed')
    })
  })
}

// notes
export const fetchNotes = (context) => {
  context.commit('setLoading', true)

  return new Promise(resolve => {
    return Note.fetchOwnList().then(results => {
      const notesData = results.map(elm => parseNote(elm))
      context.commit('setNotes', notesData)
      return resolve(notesData)
    }).finally(() => {
      context.commit('setLoading', false)
    })
  })
}

export const parseNote = (rawNote) => {
  const noteData = rawNote.attrs
  noteData.id = rawNote._id
  noteData._id = rawNote._id
  noteData.serializedData = JSON.parse(noteData.data)
  noteData.serializedData.plainContent = new JSSoup(noteData.serializedData.content).getText()
  return {
    createdAt: noteData.createdAt,
    updatedAt: noteData.updatedAt,
    id: noteData.id,
    _id: noteData.id,
    ...noteData.serializedData
  }
}

export const parseData = (rawNote) => {
  const noteData = rawNote.attrs
  noteData.id = rawNote._id
  noteData._id = rawNote._id
  noteData.serializedData = JSON.parse(noteData.data)
  return {
    createdAt: noteData.createdAt,
    updatedAt: noteData.updatedAt,
    id: noteData.id,
    _id: noteData.id,
    ...noteData.serializedData
  }
}

export const createNote = (context, data) => {
  context.commit('setLoading', true)

  return new Promise(resolve => {
    const noteData = new Note({ data: JSON.stringify(data) })

    return noteData.save().then((note) => {
      return resolve(parseNote(note))
    }).finally(() => {
      context.dispatch('fetchNotes')
    })
  })
}

export const deleteNote = (context, noteId) => {
  context.commit('setLoading', true)

  return new Promise(resolve => {
    return Note.findById(noteId).then(note => note.destroy()).then(resolve).finally(() => {
      context.dispatch('fetchNotes')
    })
  })
}

//
export const updateNote = (context, { id, data }) => {
  context.commit('setLoading', true)

  return new Promise(resolve => {
    return Note.findById(id).then(note => {
      note.update({ data: JSON.stringify(data) })
      return note.save()
    }).then(note => resolve(parseNote(note))).finally(() => {
      context.dispatch('fetchNotes')
    })
  })
}

// file

export const constructFileRecord = (fileRecord) => {
  const data = parseData(fileRecord)
  data.serverId = data.id
  return data
}

export const fetchFiles = (context) => {
  return new Promise(resolve => {
    return FileRecord.fetchOwnList().then(results => {
      const fileData = results.map(elm => constructFileRecord(elm))
      context.commit('setFiles', fileData)
      return resolve(fileData)
    })
  })
}

export const createFile = (context, fileInfo) => {
  return new Promise(resolve => {
    const fileData = new FileRecord({ data: JSON.stringify(fileInfo) })

    return fileData.save().then((note) => {
      return resolve(constructFileRecord(note))
    })
  })
}

export const updateFileStatus = (context, { id, status = 'done' }) => {
  return new Promise(resolve => {
    return FileRecord.findById(id).then(fileRecord => {
      const currentData = JSON.parse(fileRecord.attrs.data)

      currentData.fileStatus = status

      fileRecord.update({ data: JSON.stringify(currentData) })
      return fileRecord.save()
    }).then(() => {
      return context.commit('updateStatusFileLocal', { id, status })
    })
      .then(resolve)
  })
}

export const deleteFileRecord = (context, fileRecordObject) => {
  return new Promise(resolve => {
    const fileRecordId = fileRecordObject.fileId
    const fileRecordRadiksId = fileRecordObject.id

    deleteChunkedBuffer(fileRecordId, fileRecordObject.fragments).finally(() => {
      FileRecord.findById(fileRecordRadiksId)
        .then(fileRecord => fileRecord.destroy())
        // .finally(() => {
        //   return context.commit('deleteFileLocal', fileRecordRadiksId)
        // })
        .then(resolve)
    })
  })
}
