import { isEmpty } from 'ramda'
import { AppUserSession } from '../../services/radiks'

const prettyFileIcons = require('pretty-file-icons')
const sampleAvatar = 'https://privakeeper.com/common_avatar.png'

export function getter (state) {

}

export const isUserSignedIn = (state) => {
  return !isEmpty(state.user) && AppUserSession.isUserSignedIn()
}

export const userAvatar = (state) => {
  if (!(!isEmpty(state.user) && state.user.profile.image && !isEmpty(state.user.profile.image))) return sampleAvatar

  const images = state.user.profile.image

  const avatar = images.filter(elm => elm.name === 'avatar')

  if (avatar) return avatar[0].contentUrl

  return sampleAvatar
}

export const userDisplayName = (state) => {
  if (isEmpty(state.user)) return ''
  try {
    return state.user.profile.name ? state.user.profile.name : state.user.username.split('.')[0]
  } catch (e) {
    return state.user.identityAddress
  }
}

export const twoFactorSeedsView = (state) => {
  const twoFactorSeeds = state.twoFactorSeeds.filter(elm => JSON.stringify(elm).toLowerCase().includes(state.searchTerm.toLowerCase())) // already sorted date descending

  twoFactorSeeds.sort((elm1, elm2) => elm2.createdAt - elm1.createdAt)

  const folders = twoFactorSeeds // this also sorted by date descending
    .map(elm => elm.folder)
    .filter((elm, index, currentArray) => {
      return index === currentArray.indexOf(elm)
    })

  // folders.sort()

  const result = folders.map(folder => {
    const computedData = twoFactorSeeds.filter(elm => elm.folder === folder)
    computedData.sort((elm1, elm2) => elm2.createdAt - elm2.createdAt)
    return [folder, computedData]
  })

  return result
}

export const twoFactorSeedsFolder = (state) => {
  const dataset = twoFactorSeedsView(state)

  return dataset.map(elm => [elm[0], elm[1].length])
}

export const passwordSeedsView = (state) => {
  const passwordSeeds = state.passwordSeeds.filter(elm => JSON.stringify(elm).toLowerCase().includes(state.searchTerm.toLowerCase())) // already sorted date descending

  passwordSeeds.sort((elm1, elm2) => elm2.createdAt - elm1.createdAt)

  const folders = passwordSeeds // this also sorted by date descending
    .map(elm => elm.folder)
    .filter((elm, index, currentArray) => {
      return index === currentArray.indexOf(elm)
    })

  // folders.sort()

  const result = folders.map(folder => {
    const computedData = passwordSeeds.filter(elm => elm.folder === folder)
    computedData.sort((elm1, elm2) => elm2.createdAt - elm2.createdAt)
    return [folder, computedData]
  })

  return result
}

export const passwordSeedsFolder = (state) => {
  const dataset = passwordSeedsView(state)

  return dataset.map(elm => [elm[0], elm[1].length])
}

export const notesDataView = (state) => {
  const pinnedNotes = state.notes
    .filter(elm => elm.pinned === true)

  pinnedNotes.sort((elm1, elm2) => elm2.updatedAt - elm1.updatedAt)

  const unPinnedNotes = state.notes
    .filter(elm => elm.pinned !== true)

  unPinnedNotes.sort((elm1, elm2) => elm2.updatedAt - elm1.updatedAt)

  const notes = pinnedNotes.concat(unPinnedNotes)
    .filter(elm => JSON.stringify(elm).toLowerCase().includes(state.searchTerm.toLowerCase())) // already sorted date descending

  return notes
}

// file
const parseFileView = (fileInfo) => {
  return {
    ...fileInfo,

    // set type to local to indicate an already uploaded file
    options: {
      type: 'local',

      // stub file information
      file: {
        name: fileInfo.fileName,
        size: fileInfo.fileSize,
        type: fileInfo.fileType
      },

      // pass poster property
      metadata: {
        poster: require(`pretty-file-icons/svg/${prettyFileIcons.getIcon(fileInfo.fileName, 'svg')}`)
      }
    }
  }
}

export const fileView = (state) => {
  const files = state.files.map(fileInfo => parseFileView(fileInfo))
    .filter(elm => JSON.stringify(elm).toLowerCase().includes(state.searchTerm.toLowerCase()))

  files.sort((elm1, elm2) => elm2.createdAt - elm1.createdAt)

  return files
}

const humanFileSize = (bytes, si) => {
  var thresh = si ? 1000 : 1024
  if (Math.abs(bytes) < thresh) {
    return bytes + ' B'
  }
  var units = si
    ? ['kB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB']
    : ['KiB', 'MiB', 'GiB', 'TiB', 'PiB', 'EiB', 'ZiB', 'YiB']
  var u = -1
  do {
    bytes /= thresh
    ++u
  } while (Math.abs(bytes) >= thresh && u < units.length - 1)
  return bytes.toFixed(1) + ' ' + units[u]
}

export const summaryFileUsage = (state) => {
  const files = state.files

  const totalStorage = humanFileSize(files.reduce((accumulator, elm) => {
    return accumulator + elm.fileSize
  }, 0), true)

  return totalStorage
}

export const progressBarData = (state) => {
  const progressData = Object.keys(state.progressData).map(key => state.progressData[key])

  progressData.sort((elm1, elm2) => elm2.time - elm1.time)

  return progressData
}

export const progressBarDataWithoutDownload = (state) => {
  const progressData = progressBarData(state)

  return progressData.filter(elm => elm.status !== 'downloading')
}
