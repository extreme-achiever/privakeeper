import { flushStates } from './state'

export const setSearchTerm = (state, searchTerm) => {
  state.searchTerm = searchTerm || ''
}

export const setUser = (state, data) => {
  state.user = data
}

export const setLoading = (state, appLoading) => {
  state.appLoading = appLoading
}

export const clearState = (state) => {
  Object.assign(state, flushStates)
}

export const setRemainingOTPTime = (state, remainingOTPTime) => {
  state.remainingOTPTime = remainingOTPTime
}

export const setDrawer = (state, drawerMenu) => {
  state.drawerMenu = drawerMenu
}

// two factor authentication
export const setTwoFactorSeed = (state, passwordSeeds) => {
  state.twoFactorSeeds = passwordSeeds
}

export const setTwoLayoutConfigLayoutMode = (state, layoutMode) => {
  state.twoLayoutConfig_layoutMode = layoutMode
}

export const setTwoLayoutConfigVisibility = (state, visibility) => {
  state.twoLayoutConfig_visibility = visibility
}

// password
export const setPasswordSeed = (state, twoFactorSeeds) => {
  state.passwordSeeds = twoFactorSeeds
}

export const setPasswordConfigLayoutMode = (state, layoutMode) => {
  state.passwordLayoutConfig_layoutMode = layoutMode
}

export const setPasswordConfigVisibility = (state, visibility) => {
  state.passwordLayoutConfig_visibility = visibility
}

// notes
export const setNotes = (state, notes) => {
  state.notes = notes
}

// files

export const setFileLayoutConfigLayoutMode = (state, layoutMode) => {
  state.fileLayoutConfig_layoutMode = layoutMode
}

export const setFiles = (state, files) => {
  state.files = files
}

export const setFileLoading = (state, fileLoading) => {
  state.files = fileLoading
}

export const updateStatusFileLocal = (state, { id, status }) => {
  const files = state.files

  const filteredFiles = files.filter(elm => elm.id === id)

  if (filteredFiles.length > 0) filteredFiles[0].fileStatus = status
}

export const deleteFileLocal = (state, fileId) => {
  const files = state.files

  const fileIndex = files.map(elm => elm.id).indexOf(fileId)

  if (fileIndex !== -1) files.splice(fileIndex, 1)
}

export const resetProgress = (state) => {
  state.progressData = {}
}

export const setFileToProgressBar = (state, { fileId, fileName, status }) => {
  state.progressData[fileId] = { fileId, fileName, status, time: new Date().getTime() }

  state.progressData = JSON.parse(JSON.stringify(state.progressData))
}

export const removeProgress = (state, { fileId }) => {
  const progressData = state.progressData

  delete progressData[fileId]

  state.progressData = JSON.parse(JSON.stringify(state.progressData))
}
