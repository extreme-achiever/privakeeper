const initialStates = {
  user: {},
  appLoading: false,
  searchTerm: '',
  remainingOTPTime: 60,
  drawerMenu: [
    {
      path: '/private-file',
      name: 'Private Files',
      icon: 'cloud_done'
    },
    {
      path: '/note',
      name: 'Private Notes',
      icon: 'note'
    },
    {
      path: '/2fa',
      name: 'Private 2FAs',
      icon: 'timelapse'
    },
    {
      path: '/password',
      name: 'Private Passwords',
      icon: 'lock'
    }
  ],

  twoLayoutConfig_layoutMode: 'thumbnail',
  twoLayoutConfig_visibility: false,
  twoFactorSeeds: [],

  passwordLayoutConfig_layoutMode: 'thumbnail',
  passwordLayoutConfig_visibility: false,
  passwordSeeds: [],

  notes: [],

  files: [],
  fileLayoutConfig_layoutMode: 'thumbnail',
  fileLoading: false,
  progressData: {}
}

const flushStates = JSON.parse(JSON.stringify(initialStates))

delete flushStates['appLoading']
delete flushStates['searchTerm']
delete flushStates['fileLoading']
delete flushStates['progressData']

export default initialStates

export {
  flushStates
}
