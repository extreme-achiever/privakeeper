import Vue from 'vue'
import Vuex from 'vuex'

import app from './app'
import createPersistedState from 'vuex-persistedstate'
// import VuexUndoRedo from 'vuex-undo-redo'

Vue.use(Vuex)
// Vue.use(VuexUndoRedo)

/*
 * If not building with SSR mode, you can
 * directly export the Store instantiation
 */
const Store = new Vuex.Store({
  modules: {
    app
  },
  plugins: [createPersistedState()],
  // enable strict mode (adds overhead!)
  // for dev mode only
  strict: process.env.DEV
})

export default function (/* { ssrContext } */) {
  return Store
}

export { Store }
