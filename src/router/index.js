import Vue from 'vue'
import VueRouter from 'vue-router'

import routes from './routes'
import { Store } from '../store'

Vue.use(VueRouter)

/*
 * If not building with SSR mode, you can
 * directly export the Router instantiation
 */

export default function (/* { store, ssrContext } */) {
  const Router = new VueRouter({
    scrollBehavior: () => ({ x: 0, y: 0 }),
    routes,

    // Leave these as is and change from quasar.conf.js instead!
    // quasar.conf.js -> build -> vueRouterMode
    // quasar.conf.js -> build -> publicPath
    mode: process.env.VUE_ROUTER_MODE,
    base: process.env.VUE_ROUTER_BASE
  })
  //
  Router.beforeEach((to, from, next) => {
    console.log(Store.getters['app/isUserSignedIn'])
    console.log(to.path === '/login')

    Store.commit('app/setSearchTerm', '')

    if (to.meta.requiredLogin === true) {
      // in case of not logged in
      if (Store.getters['app/isUserSignedIn'] === false) return next('/login')
    } else {
      // in case of already logged in
      if (Store.getters['app/isUserSignedIn'] === true && to.path === '/login') return next('/home')
    }
    return next()
  })

  return Router
}
