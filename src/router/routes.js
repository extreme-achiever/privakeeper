const routes = [
  {
    path: '',
    component: () => import('layouts/Landing.vue'),
    meta: {
      name: 'Landing'
    }
  },
  {
    path: '/',
    component: () => import('layouts/MainApp.vue'),
    children: [
      {
        path: 'home',
        component: () => import('layouts/MainApp.vue'),
        meta: {
          name: 'Home'
        }
      },
      {
        path: '2fa',
        component: () => import('pages/2FA.vue'),
        meta: {
          name: 'Private 2FAs',
          requiredLogin: true
        }
      },
      {
        path: 'account',
        component: () => import('pages/Account.vue'),
        meta: {
          name: 'Account',
          requiredLogin: true
        }
      },
      {
        path: 'note',
        component: () => import('pages/Note.vue'),
        meta: {
          name: 'Private Notes',
          requiredLogin: true
        }
      },
      {
        path: 'password',
        component: () => import('pages/Password.vue'),
        meta: {
          name: 'Private Passwords',
          requiredLogin: true
        }
      },
      {
        path: 'private-file',
        component: () => import('pages/PrivateFile.vue'),
        meta: {
          name: 'Private Files',
          requiredLogin: true
        }
      },
      {
        path: 'term',
        component: () => import('pages/Term.vue'),
        meta: {
          name: 'Terms and Privacy Policy'
        }
      }
    ],
    meta: {
      name: 'Home'
    }
  },
  {
    path: '/login',
    component: () => import('layouts/Login.vue'),
    meta: {
      name: 'Login'
    }
  }
]

// Always leave this as last one
if (process.env.MODE !== 'ssr') {
  routes.push({
    path: '*',
    component: () => import('pages/Error404.vue')
  })
}

export default routes
