import { Authenticator } from 'otplib/authenticator'

import { keyDecoder, keyEncoder } from 'otplib/plugin-thirty-two'

import { createDigest, createRandomBytes } from 'otplib/plugin-crypto-js'

export const authenticator = new Authenticator({
  createDigest,
  createRandomBytes,
  keyDecoder,
  keyEncoder
})

authenticator.options = {
  // window: 1
  // epoch: new Date().getTime()
}

export const generateToken = (secret) => {
  try {
    const serializedSecret = secret.split('').filter(elm => elm !== ' ').join('')
    return authenticator.generate(serializedSecret)
  } catch (err) {
    return 'Error'
  }
}

export const generateSecret = () => authenticator.generateSecret()
