import { getConfig, configure, User as AppUser } from 'radiks'
import { AppConfig, UserSession } from 'blockstack'

const userSession = new UserSession({
  appConfig: new AppConfig(['store_write', 'publish_data'])
})

configure({
  apiServer: 'https://backend.privakeeper.com',
  userSession
})

const { userSession: AppUserSession } = getConfig()

export {
  AppUserSession,
  AppUser,
  getConfig
}
