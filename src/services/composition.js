import { createComponent } from '@vue/composition-api'
import { QTooltip, QBtn } from 'quasar'
import AppSpinner from '../components/Utils/AppSpinner'
import { downloadLargeFile } from './utils'

export const FileActionsComponent = createComponent({
  data () {
    return {
      loading: false,
      text: ''
    }
  },
  props: {
    file: Object,
    default: () => {
    }
  },
  methods: {
    deleteFile () {
      const confirmDeleteFile = confirm(`You want to delete ${this.file.source.fileName} ?`)

      if (confirmDeleteFile === false) return
      this.loading = true
      this.text = 'Deleting'
      this.$store.commit('app/setFileToProgressBar', {
        fileId: this.file.source.fileId,
        status: 'deleting',
        fileName: this.file.source.fileName
      })
      this.$store.dispatch('app/deleteFileRecord', this.file.source).finally(() => {
        this.$emit('removeFile', this.file.source.fileId)
        this.$store.commit('app/removeProgress', { fileId: this.file.source.fileId })
      })
    },
    downloadFile () {
      this.loading = true
      this.text = 'Downloading'

      this.$store.commit('app/setFileToProgressBar', {
        fileId: this.file.source.fileId,
        status: 'Downloading',
        fileName: this.file.source.fileName
      })

      downloadLargeFile(this.file.source).finally(() => {
        this.loading = false
        this.$store.commit('app/removeProgress', { fileId: this.file.source.fileId })
      })
    }
  },
  render () {
    if (this.file.serverId === null) return null
    if (this.file.source.fileStatus === 'not-done') {
      return (
        <div class="absolute-center" style={{ zIndex: 9999 }}>
          <div class="row col-12 justify-center q-mt-md">
            <QBtn icon="delete" size="sm" color="red" v-on:click={this.deleteFile} class="q-pa-none q-mr-sm">
              <QTooltip>
                Delete {this.file.filename || this.file.source.fileName}
              </QTooltip>
            </QBtn>
          </div>
          <AppSpinner text={'Completing'}/>
        </div>
      )
    }
    return (
      <div class="absolute-center" style={{ zIndex: 9999 }}>
        {this.loading === false ? (<div class="row col-12 justify-center" style={{ width: '150px' }}>
          <QBtn icon="delete" size="sm" color="red" v-on:click={this.deleteFile} class="q-pa-none q-mr-sm">
            <QTooltip>
                Delete {this.file.filename || this.file.source.fileName}
            </QTooltip>
          </QBtn>
          <QBtn icon="get_app" size="sm" color="primary" v-on:click={this.downloadFile} class="q-pa-none">
            <QTooltip>
                Download {this.file.filename || this.file.source.fileName}
            </QTooltip>
          </QBtn>
        </div>)
          : (<AppSpinner text={this.text}/>)}
      </div>
    )
  }
})
