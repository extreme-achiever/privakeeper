import { Model } from 'radiks'

export class TwoFactorSeed extends Model {
  static className = 'TwoFactorSeed';
  static schema = { // all fields are encrypted by default
    data: {
      type: [String, null],
      decrypted: false
    }
  }
}

export class PasswordSeed extends Model {
  static className = 'PasswordSeed';
  static schema = { // all fields are encrypted by default
    data: {
      type: [String, null],
      decrypted: false
    }
  }
}

export class Note extends Model {
  static className = 'Note';
  static schema = { // all fields are encrypted by default
    data: {
      type: [String, null],
      decrypted: false
    }
  }
}

export class FileRecord extends Model {
  static className = 'FileRecord';
  static schema = { // all fields are encrypted by default
    data: {
      type: [String, null],
      decrypted: false
    }
  }
}
