export { default as RandomColor } from 'randomcolor'
import { AppUserSession } from './radiks'
import _ from 'lodash'
import { uid } from 'quasar'
import * as WebStreamsPolyfill from 'web-streams-polyfill/ponyfill'

window.WebStreamsPolyfill = WebStreamsPolyfill
import streamSaver from 'streamsaver'

import { Readable } from 'stream'

// export const MAX_FILE_SIZE = 1.5e+7 // 15Mb
export const MAX_FILE_SIZE = 2.0e+7 // 20Mb
export const MAX_CONCURRENT_LIMIT = 5
//
// export const getFileInfo = (file) => {
//   return {
//     fileId: file.id,
//     fileName: file.filename,
//     fileNameWithoutExtension: file.filenameWithoutExtension,
//     fileExtension: file.fileExtension,
//     fileSize: file.fileSize,
//     fileType: file.fileType
//   }
// }

export const constructFileRecord = (fileNative) => {
  const fileNameArray = fileNative.name.split('.')

  const fileExtension = fileNameArray.length >= 2 ? fileNameArray.pop() : ''

  return {
    fileId: uid().toString(),
    fileName: fileNative.name,
    fileNameWithoutExtension: fileNameArray.join('.'),
    fileExtension,
    fileSize: fileNative.size,
    fileType: fileNative.type,
    file: fileNative,
    fileStatus: 'not-done'
  }
}

export const batchUpload = (fileId, chunksBuffer, progressHandler) => {
  const promises = chunksBuffer
    .map(({ order, chunkedBuffer }) => () =>
      new Promise(resolve => AppUserSession.putFile(`${fileId}_${order}`, chunkedBuffer, { encrypt: false })
        .then(() => {
          progressHandler()
          resolve()
        }))
    )
  return new Promise((resolve, reject) => {
    Promise.all(promises.map(processUpload => processUpload())).then(resolve).catch(reject)
  })
}

export const extractFileAndBuffer = (fileInfo) => {
  return new Promise((resolve) => {
    // const fileInfo = getFileInfo(file)
    fileInfo.file.arrayBuffer().then(async fileBuffer => {
      delete fileInfo['file']
      return resolve({
        fileInfo,
        fileBuffer
      })
    })
  })
}

export const convertFileBufferToBase64 = (buffer, contentType) => {
  // https://stackoverflow.com/a/49124600/6182992
  const base64 = btoa(new Uint8Array(buffer).reduce(function (data, byte) {
    return data + String.fromCharCode(byte)
  }, ''))

  return `data:${contentType};base64,${base64}`
}

// legacy: case handle all upload/downloda in app side
export const chunkFilesAndUpload = (fileInfo, filePondBuffer) => {
  return new Promise(async (resolve) => {
    const fileBuffer = Buffer.from(filePondBuffer)

    const chunksBuffer = _.chunk(fileBuffer, MAX_FILE_SIZE)

    const promises = chunksBuffer
      .map((buffer, index) => () =>
        new Promise(resolve => AppUserSession.putFile(`${fileInfo.fileId}_${index}`, buffer).then(resolve))
      )

    const batches = _.chunk(promises, MAX_CONCURRENT_LIMIT)

    for (var index in batches) {
      console.log(index)
      await Promise.all(batches[index].map(uploadProcess => uploadProcess()))
    }
    return resolve({
      ...fileInfo,
      fragments: chunksBuffer.length
    })
  })
}

export const deleteChunkedBuffer = async (fileId, fragments) => {
  return new Promise(async (resolve) => {
    const fragmentArray = [...Array(fragments).keys()].map(index => `${fileId}_${index}`)

    const promises = fragmentArray.map(fileName => () => AppUserSession.deleteFile(fileName))

    const batches = _.chunk(promises, MAX_CONCURRENT_LIMIT)

    for (var index in batches) {
      try {
        await Promise.all(batches[index].map(deleteProcess => deleteProcess()))
      } catch (er) {
        console.log(er)
      }
    }
    return resolve()
  })
}
export const queryFile = (fileInfo) => {
  return new Promise(async (resolve) => {
    const requestOrders = [...Array(fileInfo.fragments).keys()]
    console.log(requestOrders)

    const promises = requestOrders
      .map((index) => () =>
        new Promise(resolve => AppUserSession.getFile(`${fileInfo.fileId}_${index}`, { decrypt: true }).then(resolve))
      )

    const batches = _.chunk(promises, MAX_CONCURRENT_LIMIT)

    let bufferArrays = []
    for (var index in batches) {
      let batchBufferArrays = await Promise.all(batches[index].map(uploadProcess => uploadProcess()))

      let batchBuffer = Buffer.concat(batchBufferArrays)

      bufferArrays.push(batchBuffer)
    }

    return resolve({
      ...fileInfo,
      bufferArrays
    })
  })
}

const convertBufferToStream = (buffer) => {
  const stream = new Readable()
  stream.push(buffer)
  stream.push(null)
  return stream
}

const writeToStream = (streamWriter, stream) => {
  return new Promise(async (resolve) => {
    const value = await stream.read()
    console.log(value)
    streamWriter.write(value).then(resolve)
  })
}

const initUnload = (streamWriter, statusOject) => {
  // abort so it dose not look stuck
  window.onunload = () => {
    // also possible to call abort on the writer you got from `getWriter()`
    streamWriter.abort()
  }

  window.onbeforeunload = evt => {
    if (statusOject.status !== 'done') {
      evt.returnValue = `Are you sure you want to leave?`
    }
  }
}
export const downloadLargeFile = (fileInfo) => {
  return new Promise(async (resolve) => {
    const fileStream = streamSaver.createWriteStream(`${fileInfo.fileName}`, fileInfo.fileSize)
    const writeStream = fileStream.getWriter()

    const requestOrders = [...Array(fileInfo.fragments).keys()]

    const statusObject = {
      status: 'started'
    }

    initUnload(writeStream, statusObject)

    console.log(requestOrders)

    const promises = requestOrders
      .map((index) => () =>
        new Promise(resolve => AppUserSession.getFile(`${fileInfo.fileId}_${index}`, { decrypt: false }).then(resolve))
      )

    const batches = _.chunk(promises, MAX_CONCURRENT_LIMIT)

    for (var index in batches) {
      let batchBufferArrays = await Promise.all(batches[index].map(uploadProcess => uploadProcess()))

      let batchBuffer = Buffer.concat(batchBufferArrays.map(elm => Buffer.from(elm)))

      const stream = convertBufferToStream(batchBuffer)

      await writeToStream(writeStream, stream)
    }

    writeStream.close()
    statusObject.status = 'done'

    return resolve({
      ...fileInfo,
      status: statusObject.status
    })
  })
}
